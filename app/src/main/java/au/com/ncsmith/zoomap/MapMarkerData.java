package au.com.ncsmith.zoomap;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by NickSmith on 27/03/2017.
 */

public class MapMarkerData {


    public LatLng Location;
    private String Name;
    private String Summary;
    private String Question;
    private int CorrectAnswer;
    private String[] Answers;
    private PopupWindow quizWindow;
    private boolean Answered;

    public MapMarkerData(JSONObject obj)
    {
        try {
            Name = obj.getString("name");
            Summary = obj.getString("summary");
            Question = obj.getString("question");
            CorrectAnswer = obj.getInt("correct_answer");
            Location = new LatLng(obj.getDouble("lat"), obj.getDouble("lng"));
            Answered = false;

            // Get answers
            Answers = new String[3];
            JSONArray answers = obj.getJSONArray("answers");
            for (int i=0; i < answers.length(); i++)
            {
                JSONObject answer = answers.getJSONObject(i);
                Answers[i] = answer.getString("answer");
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void ConfigureWindow(final Context parent, RelativeLayout rootLayout, final Marker marker)
    {
        if(Answered)
        {
            Toast.makeText(parent, "You've already answered this one!", Toast.LENGTH_LONG).show();
            return;
        }


        LayoutInflater inflater = (LayoutInflater) parent.getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.quiz_layout, null);


        TextView titleText = (TextView) customView.findViewById(R.id.titleText);
        titleText.setText(this.Name);
        TextView SummaryText = (TextView) customView.findViewById(R.id.summaryText);
        SummaryText.setText(this.Summary);

        quizWindow = new PopupWindow(customView,
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        if(Build.VERSION.SDK_INT >= 21)
        {
            quizWindow.setElevation(5.0f);
        }

        Button option1 = (Button) customView.findViewById(R.id.answer1);
        option1.setText(Answers[0]);
        Button option2 = (Button) customView.findViewById(R.id.answer2);
        option2.setText(Answers[1]);
        Button option3 = (Button) customView.findViewById(R.id.answer3);
        option3.setText(Answers[2]);

        // Set a click listener for the popup window close button

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CorrectAnswer == 1)
                {
                    quizWindow.dismiss();
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.complete_marker));
                    Answered = true;
                }
                else Toast.makeText(parent, "Wrong answer!", Toast.LENGTH_LONG).show();
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CorrectAnswer == 2)
                {
                    quizWindow.dismiss();
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.complete_marker));
                    Answered = true;
                }
                else Toast.makeText(parent, "Wrong answer!", Toast.LENGTH_LONG).show();
            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CorrectAnswer == 3)
                {
                    quizWindow.dismiss();
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.complete_marker));
                    Answered = true;
                }
                else Toast.makeText(parent, "Wrong answer!", Toast.LENGTH_LONG).show();
            }
        });
        quizWindow.showAtLocation(rootLayout, Gravity.CENTER, 0, 0);


    }
}
