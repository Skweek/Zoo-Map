package au.com.ncsmith.zoomap;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ZooMapActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    private GoogleMap mMap;
    private RelativeLayout rootLayout;
    private List<MapMarkerData> Markers;
    private LatLng Zoo;
    private LatLngBounds ZooBounds; // SW, NE
    private int LocationData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_melbourne_map);
        rootLayout = (RelativeLayout)findViewById(R.id.rl);

        // Get our extra info
        Bundle extras = getIntent().getExtras();
        
        Zoo = ((LatLng) extras.get("center"));
        ZooBounds = ((LatLngBounds) extras.get("boundary"));
        LocationData = extras.getInt("data");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Markers = new ArrayList<>();

        // Get markers
        try {
            InputStream is = getResources().openRawResource(LocationData);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            try {
                JSONArray array = new JSONObject(json).getJSONArray("locations");
                for (int i=0; i < array.length(); i++)
                {
                    try {
                        JSONObject obj = array.getJSONObject(i);
                        Markers.add(new MapMarkerData(obj));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MapStyleOptions styleOptions = new MapStyleOptions(getResources().getString(R.string.map_style_json));
        boolean style = mMap.setMapStyle(styleOptions);
        if (!style)
        {
            Log.e("Error", "Style parsing failed.");
        }

        for (int i=0; i < Markers.size(); i++)
        {
            Marker mark = mMap.addMarker(new MarkerOptions().position(Markers.get(i).Location));
            mark.setTag(i);
            mark.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.question_marker));
        }

        CameraPosition position = CameraPosition.builder()
                .target(Zoo)
                .zoom(19f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMinZoomPreference(18f);
        mMap.setLatLngBoundsForCameraTarget(ZooBounds);

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), null);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        int permission = this.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        if(permission == PackageManager.PERMISSION_GRANTED)
        {
            mMap.setMyLocationEnabled(true);
        }
        else
        {
            Toast.makeText(this, "Error with permission!", Toast.LENGTH_LONG).show();
        }
        // Set a listener for marker click.
        mMap.setOnMarkerClickListener(this);
    }

    /** Called when the user clicks a marker. */
    @Override
    public boolean onMarkerClick(final Marker marker) {


        // Inflate the custom layout/view
        int Index = (int)marker.getTag();
        Markers.get(Index).ConfigureWindow(getBaseContext(), rootLayout, marker);

        return true;
    }
}
