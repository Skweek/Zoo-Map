package au.com.ncsmith.zoomap;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import static android.R.attr.value;

public class MainActivity extends AppCompatActivity {

    static private final int LOCATION_REQUEST_CODE = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int p = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (p != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
        else
        {
            RunApp();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    RunApp();
                }
                else
                {
                    Toast.makeText(this, "Permission for location services denied!", Toast.LENGTH_LONG).show();
                    // TO DO, enable non gps game play
                }

            }
        }
    }

    private void RunApp()
    {
        Button melbZoo = (Button) findViewById(R.id.melbourne_zoo);
        melbZoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ZooMapActivity.class);
                myIntent.putExtra("center", new LatLng(-37.7841346, 144.9515473));
                myIntent.putExtra("boundary", new LatLngBounds(new LatLng(-37.78833,144.9469533), new LatLng(-37.781253,144.9537013)));
                myIntent.putExtra("data", R.raw.melbourne_locations);
                MainActivity.this.startActivity(myIntent);
            }
        });
        Button werrZoo = (Button) findViewById(R.id.werribee_zoo);
        werrZoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ZooMapActivity.class);
                myIntent.putExtra("center", new LatLng(-37.92116, 144.6673052));
                myIntent.putExtra("boundary", new LatLngBounds(new LatLng(-37.925691,144.6567163), new LatLng(-37.918143,144.6654861)));
                myIntent.putExtra("data", R.raw.werribee_locations);
                MainActivity.this.startActivity(myIntent);
            }
        });
        Button healZoo = (Button) findViewById(R.id.healesville_zoo);
        healZoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ZooMapActivity.class);
                myIntent.putExtra("center", new LatLng(-37.7841346, 144.9515473));
                myIntent.putExtra("boundary", new LatLngBounds(new LatLng(-37.78833,144.9469533), new LatLng(-37.781253,144.9537013)));
                myIntent.putExtra("data", R.raw.healesville_locations);
                MainActivity.this.startActivity(myIntent);
            }
        });
    }
}
